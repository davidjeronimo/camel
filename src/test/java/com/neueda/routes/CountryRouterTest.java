package com.neueda.routes;

import com.neueda.dto.Country;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CountryRouterTest {
    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private ConsumerTemplate consumerTemplate;

    @Test
    public void test() {
        List<Country> countries = producerTemplate.requestBody("direct:allCountries", null, List.class);
        assertEquals(250, countries.size());
        assertEquals("Afghanistan", countries.get(0).getName());
    }

    @Test
    public void testCountry() {
        Country country = producerTemplate.requestBody("direct:country", "ch", Country.class);
        assertNotNull(country);
        assertEquals("Switzerland", country.getName());
        assertEquals(3, country.getCurrencies().size());
    }
}
