package com.neueda.routes;

import com.example.tutorial.CountryFxProtos.CountryExchangeRatesProtobuf;
import com.neueda.routes.embeddedBroker.EmbeddedQpidBrokerRule;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RabbitMqRouterTest {
    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private ConsumerTemplate consumerTemplate;

    @ClassRule
    public static EmbeddedQpidBrokerRule qpidBrokerRule = new EmbeddedQpidBrokerRule();

    @Test
    public void test() throws Exception {

        CompletableFuture<CountryExchangeRatesProtobuf> eventualResult = CompletableFuture.supplyAsync(
                () -> consumerTemplate.receive("vm:europe", 1000).getIn().getBody(CountryExchangeRatesProtobuf.class)
        );
        producerTemplate.sendBody("direct:publish", CountryExchangeRatesProtobuf.newBuilder().
                setAverage(3.5)
                .setRegion("Europe")
                .setName("Switzerland")
                .build()
        );
        CountryExchangeRatesProtobuf result = eventualResult.get(1, TimeUnit.SECONDS);
        assertNotNull(result);
        assertEquals(3.5, result.getAverage(), 0);

    }

}
