package com.neueda.routes;

import com.neueda.model.CountryFxData;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RouterTest {
    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private ConsumerTemplate consumerTemplate;

    @Test
    public void testCountryData() {
        CountryFxData result = producerTemplate.requestBody("direct:countryData", "es", CountryFxData.class);
        assertNotNull(result);
        assertEquals(5, result.getBorderCountriesExchangeRates().size());
    }

    @Test
    public void testCountryData_multipleCurrencies() {
        CountryFxData result = producerTemplate.requestBody("direct:countryData", "ch", CountryFxData.class);
        assertNotNull(result);
        assertEquals(5, result.getBorderCountriesExchangeRates().size());
    }
}
