package com.neueda.routes;

import com.neueda.dto.CurrencyExchangeDTO;
import com.neueda.model.ExchangeRate;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FxRouterTest {
    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private ConsumerTemplate consumerTemplate;


    @Test
    public void testCountry() {
        ExchangeRate exchangeRate = new ExchangeRate("USD", "JPY");
        CurrencyExchangeDTO result = producerTemplate.requestBody("direct:fx", exchangeRate, CurrencyExchangeDTO.class);
        assertNotNull(result);
        assertEquals("USD", result.getFromCurrency());
        assertNotNull(result.getExchangeRate());
    }
}
