package com.neueda.service;

import com.example.tutorial.CountryFxProtos;
import com.example.tutorial.CountryFxProtos.CountryExchangeRatesProtobuf;
import com.neueda.model.CountryFxData;
import com.neueda.model.ExchangeRate;
import org.apache.camel.Body;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class CountryDataService {

    public CountryFxData createCountryFxData(final @Body String code) {
        CountryFxData countryFxData = new CountryFxData();
        countryFxData.setCountryCode(code);
        return countryFxData;
    }

    public List<ExchangeRate> createExchangeRateRequests(final @Body CountryFxData countryFxData) {
        return countryFxData.getCurrencies().stream().flatMap(
                fromCurrency -> countryFxData.getBorderCountriesExchangeRates().values().stream().flatMap(
                        toCurrencies -> toCurrencies.getExchangeRatesByCurrency().keySet().stream().map(
                                toCurrency -> new ExchangeRate(fromCurrency, toCurrency)
                        )
                )
        ).collect(toList());
    }

    public CountryExchangeRatesProtobuf countryFxDataToCountryExchangeRatesProtobuf(final @Body CountryFxData countryFxData) {
        return CountryFxProtos.CountryExchangeRatesProtobuf.newBuilder()
                .setName(countryFxData.getCountryName())
                .setRegion(countryFxData.getRegion())
                .setAverage(countryFxData.getAverageCurrencyComparison().orElse(0))
                .build();
    }
}
