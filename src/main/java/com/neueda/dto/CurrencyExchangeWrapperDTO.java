package com.neueda.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CurrencyExchangeWrapperDTO {
    @JsonProperty("Realtime Currency Exchange Rate")
    private CurrencyExchangeDTO currencyExchangeDTO;

    public CurrencyExchangeDTO getCurrencyExchangeDTO() {
        return currencyExchangeDTO;
    }

    public void setCurrencyExchangeDTO(CurrencyExchangeDTO currencyExchangeDTO) {
        this.currencyExchangeDTO = currencyExchangeDTO;
    }
}
