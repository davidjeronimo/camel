package com.neueda.routes;

import com.neueda.aggregator.*;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class Router extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("direct:countryData")
                .routeId("countryData")
                .bean("countryDataService", "createCountryFxData")
                .enrich("direct:enrichWithCountry", new CountryDetailsAggregator())
                .enrich("direct:borderCurrencies", new BorderCountryCurrenciesAggregator())
                .enrich("direct:exchangeRates", new ExchangeRatesAggregator())
                .wireTap("direct:sendToRabbitMq");

        from("direct:enrichWithCountry")
                .setBody(simple("${body.countryCode}"))
                .to("direct:country");


        from("direct:exchangeRates")
            .bean("countryDataService", "createExchangeRateRequests")
            .split(simple("${body}"))
                .parallelProcessing()
                .enrich("direct:fx", new ExchangeRateAggregator());


        from("direct:borderCurrencies")
                .routeId("borderCurrencies")
                .split(simple("${body.borderCountriesExchangeRates.keySet}"),new CountryCurrenciesMapAggregator())
                    .parallelProcessing()
                    .to("direct:country")
                .end();

        from("direct:sendToRabbitMq")
                .routeId("sendToRabbitMq")
                .bean("countryDataService", "countryFxDataToCountryExchangeRatesProtobuf")
                .to("direct:publish");
    }

}
