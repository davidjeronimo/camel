package com.neueda.routes;

import com.example.tutorial.CountryFxProtos;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static com.rabbitmq.client.BuiltinExchangeType.TOPIC;
import static org.apache.camel.component.rabbitmq.RabbitMQConstants.*;

@Component
public class RabbitMqRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("direct:publish")
                .routeId("publish")
                .setHeader(TYPE, constant(TOPIC.getType()))
                .setHeader(ROUTING_KEY, simple("${body.region.toLowerCase()}"))
                .setHeader(EXCHANGE_NAME, constant("exchange"))
                .marshal().protobuf()
                .to("rabbitmq://localhost/exchange");


        from("rabbitmq://localhost/exchange?routingKey=europe&autoAck=true")
                .unmarshal().protobuf(CountryFxProtos.CountryExchangeRatesProtobuf.getDefaultInstance())
                .log("###### Received message in europe queue for ${body.name}")
                .to("vm:europe");


    }
}
