package com.neueda.routes;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component
public class RestServiceRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        restConfiguration()
                .host("localhost")
                .port("8081")
                .bindingMode(RestBindingMode.json)
                .apiContextPath("/api");

        rest("/country/")
                .get()
                    .produces("application/json")
                    .to("direct:allCountries")
                .get("{code}")
                    .route()
                        .setBody(simple("${header.code}"))
                        .to("direct:country");

        rest("/fx")
                .get("{code}")
                    .route()
                        .setBody(simple("${header.code}"))
                        .to("direct:countryData");
    }


}
