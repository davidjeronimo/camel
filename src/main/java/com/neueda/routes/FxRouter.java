package com.neueda.routes;

import com.neueda.dto.CurrencyExchangeDTO;
import com.neueda.dto.CurrencyExchangeWrapperDTO;
import com.neueda.model.ExchangeRate;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import static org.apache.camel.Exchange.*;
import static org.apache.camel.component.http4.HttpMethods.GET;

@Component
public class FxRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        from("direct:fx")
                .routeId("fx")
                .choice()
                    .when(simple("${body.fromCurrency} == ${body.toCurrency}"))
                        .setBody(this::sameCurrencyExchangeDTO)
                    .otherwise()
                        .setHeader(HTTP_URI, simple("properties:fxApi.uri"))
                        .setHeader(HTTP_PATH, constant("query"))
                        .setHeader(HTTP_QUERY,
                                simple("function=CURRENCY_EXCHANGE_RATE&from_currency=${body.fromCurrency}&to_currency=${body.toCurrency}&apikey=${properties:fxApi.key}"))
                        .setHeader(HTTP_METHOD, GET)
                        .setBody(constant(null))
                        .to("http4")
                        .unmarshal().json(JsonLibrary.Jackson, CurrencyExchangeWrapperDTO.class)
                        .transform(simple("${body.currencyExchangeDTO}"))
                .end();

    }

    private CurrencyExchangeDTO sameCurrencyExchangeDTO(Exchange exchange) {
        ExchangeRate request = exchange.getIn().getBody(ExchangeRate.class);
        return new CurrencyExchangeDTO(
                request.getFromCurrency(),
                request.getToCurrency(),
                1.0
        );
    }
}
