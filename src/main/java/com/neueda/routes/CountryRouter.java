package com.neueda.routes;

import com.neueda.dto.Country;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import static org.apache.camel.Exchange.*;
import static org.apache.camel.Exchange.HTTP_QUERY;
import static org.apache.camel.component.http4.HttpMethods.GET;

@Component
public class CountryRouter extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        JacksonDataFormat format = new ListJacksonDataFormat(Country.class);

        from("direct:allCountries")
                .routeId("allCountries")
                .setHeader(HTTP_URI, simple("properties:countryApi.uri"))
                .setHeader(HTTP_PATH, constant("all"))
                .setHeader(HTTP_METHOD, GET)
                .setHeader("x-rapidapi-key", simple("properties:countryApi.key"))
                .to("http4")
                .unmarshal(format);

        from("direct:country")
                .routeId("country")
                .setHeader(HTTP_URI, simple("properties:countryApi.uri"))
                .setHeader(HTTP_PATH, simple("alpha/${body}"))
                .setHeader(HTTP_METHOD, GET)
                .setHeader("x-rapidapi-key", simple("properties:countryApi.key"))
                .log("${body}")
                .to("http4")
                .unmarshal().json(JsonLibrary.Jackson, Country.class);


    }
}
