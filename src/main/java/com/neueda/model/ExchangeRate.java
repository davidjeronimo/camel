package com.neueda.model;

import java.util.Optional;
import java.util.OptionalDouble;

public class ExchangeRate {
    private String fromCurrency;
    private String toCurrency;
    private Optional<Double> exchangeRate;

    public ExchangeRate(String fromCurrency, String toCurrency) {
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
    }

    public String getFromCurrency() {
        return fromCurrency;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public Optional<Double> getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Optional<Double> exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
