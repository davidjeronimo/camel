package com.neueda.model;

import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;

public class CountryFxData {
    private String countryCode;
    private String countryName;
    private String capital;
    private String region;
    private List<String> currencies;
    private Map<String, BorderCountryFx> borderCountriesExchangeRates;
    private OptionalDouble averageCurrencyComparison;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<String> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<String> currencies) {
        this.currencies = currencies;
    }

    public Map<String, BorderCountryFx> getBorderCountriesExchangeRates() {
        return borderCountriesExchangeRates;
    }

    public void setBorderCountriesExchangeRates(Map<String, BorderCountryFx> borderCountriesExchangeRates) {
        this.borderCountriesExchangeRates = borderCountriesExchangeRates;
    }

    public OptionalDouble getAverageCurrencyComparison() {
        return averageCurrencyComparison;
    }

    public void setAverageCurrencyComparison(OptionalDouble averageCurrencyComparison) {
        this.averageCurrencyComparison = averageCurrencyComparison;
    }
}
