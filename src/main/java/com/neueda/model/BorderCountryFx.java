package com.neueda.model;

import java.util.Map;
import java.util.OptionalDouble;

public class BorderCountryFx {
    private Map<String, OptionalDouble> exchangeRatesByCurrency;

    public BorderCountryFx(Map<String, OptionalDouble> exchangeRatesByCurrency) {
        this.exchangeRatesByCurrency = exchangeRatesByCurrency;
    }

    public Map<String, OptionalDouble> getExchangeRatesByCurrency() {
        return exchangeRatesByCurrency;
    }
}
