package com.neueda.aggregator;

import com.neueda.dto.Country;
import com.neueda.model.BorderCountryFx;
import com.neueda.model.CountryFxData;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.HashMap;
import java.util.Map;
import java.util.OptionalDouble;

import static java.util.stream.Collectors.toMap;

public class CountryDetailsAggregator implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        CountryFxData countryFxData = oldExchange.getIn().getBody(CountryFxData.class);
        Country country = newExchange.getIn().getBody(Country.class);
        countryFxData.setCapital(country.getCapital());
        countryFxData.setCountryName(country.getName());
        countryFxData.setRegion(country.getRegion());
        countryFxData.setCurrencies(country.getCurrencies());
        Map<String, BorderCountryFx> borderCountriesExchangeRates = new HashMap<>();
        for (String border : country.getBorders()) {
            borderCountriesExchangeRates.put(border, null);
        }
        countryFxData.setBorderCountriesExchangeRates(borderCountriesExchangeRates);
                /*country.getBorders().stream()
                        .collect(toMap(
                                border -> border,
                                border -> new BorderCountryFx(new HashMap<>()
                                ))
                        )
        );*/
        return oldExchange;
    }
}
