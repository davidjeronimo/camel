package com.neueda.aggregator;

import com.neueda.dto.Country;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class CountryCurrenciesMapAggregator implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        Map<String, Set<String>> borderCurrencies;
        Country country = newExchange.getIn().getBody(Country.class);
        Set<String> currencies = new HashSet<>(country.getCurrencies());
        if (oldExchange == null) {
            borderCurrencies = new HashMap<>();
            borderCurrencies.put(country.getCode(), currencies);
            newExchange.getIn().setBody(borderCurrencies);
            return newExchange;
        } else {
            borderCurrencies = oldExchange.getIn().getBody(Map.class);
            borderCurrencies.put(country.getCode(), currencies);
            return oldExchange;
        }

    }
}
