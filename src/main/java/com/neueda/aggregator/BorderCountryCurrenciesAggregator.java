package com.neueda.aggregator;

import com.neueda.model.BorderCountryFx;
import com.neueda.model.CountryFxData;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.Map;
import java.util.OptionalDouble;
import java.util.Set;

import static java.util.stream.Collectors.toMap;

public class BorderCountryCurrenciesAggregator implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        CountryFxData countryFxData = oldExchange.getIn().getBody(CountryFxData.class);
        Map<String, Set<String>> borderCountryCurrencies = newExchange.getIn().getBody(Map.class);
        countryFxData.setBorderCountriesExchangeRates(borderCountriesExchangeRates(borderCountryCurrencies));
        return oldExchange;
    }

    private Map<String, BorderCountryFx> borderCountriesExchangeRates(final Map<String, Set<String>> borderCountryCurrencies) {
        return borderCountryCurrencies.entrySet().stream()
                .collect(toMap(
                        entry -> entry.getKey(),
                        entry -> borderCountryFx(entry.getValue())));
    }

    private BorderCountryFx borderCountryFx(final Set<String> currencies) {

        return new BorderCountryFx(
                currencies.stream()
                        .collect(toMap(
                                currency -> currency,
                                currency -> OptionalDouble.empty()))
        );
    }
}
