package com.neueda.aggregator;

import com.neueda.dto.CurrencyExchangeDTO;
import com.neueda.model.ExchangeRate;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.Optional;
import java.util.OptionalDouble;

import static java.util.Optional.empty;

public class ExchangeRateAggregator implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        ExchangeRate exchangeRate = oldExchange.getIn().getBody(ExchangeRate.class);
        CurrencyExchangeDTO currencyExchangeDTO = newExchange.getIn().getBody(CurrencyExchangeDTO.class);
        final Optional<Double> rate;
        if (currencyExchangeDTO == null) {
            rate = empty();
        } else {
            rate = Optional.ofNullable(currencyExchangeDTO.getExchangeRate());
        }
        exchangeRate.setExchangeRate(rate);
        return oldExchange;
    }
}
