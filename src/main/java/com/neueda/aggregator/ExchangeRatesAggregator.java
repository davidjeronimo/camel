package com.neueda.aggregator;

import com.neueda.model.BorderCountryFx;
import com.neueda.model.CountryFxData;
import com.neueda.model.ExchangeRate;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;

public class ExchangeRatesAggregator implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        List<ExchangeRate> exchangeRates = newExchange.getIn().getBody(List.class);
        CountryFxData countryFxData = oldExchange.getIn().getBody(CountryFxData.class);
        countryFxData.getBorderCountriesExchangeRates().values().stream().forEach(
                borderCountryFx -> setExchangeRates(borderCountryFx.getExchangeRatesByCurrency(), exchangeRates)
        );

        countryFxData.setAverageCurrencyComparison(averageCurrencyComparison(countryFxData.getBorderCountriesExchangeRates()));
        return oldExchange;
    }

    private void setExchangeRates(Map<String, OptionalDouble> exchangeRatesByCurrency, List<ExchangeRate> exchangeRates) {
        exchangeRatesByCurrency.entrySet().stream().forEach(
                entry -> entry.setValue(exchangeRateToCurrency(entry.getKey(), exchangeRates))
        );
    }

    private OptionalDouble exchangeRateToCurrency(final String toCurrency, List<ExchangeRate> exchangeRates) {
        return exchangeRates.stream()
                .filter(exchangeRate -> exchangeRate.getToCurrency().equals(toCurrency))
                .filter(exchangeRate -> exchangeRate.getExchangeRate().isPresent())
                .mapToDouble(exchangeRate -> exchangeRate.getExchangeRate().get())
                .average();
    }

    private OptionalDouble averageCurrencyComparison(Map<String, BorderCountryFx> borderCountriesExchangeRates) {
        return borderCountriesExchangeRates.values().stream()
                .map(
                        borderCountryFx -> borderCountryFx.getExchangeRatesByCurrency().values().stream()
                                .filter(OptionalDouble::isPresent)
                                .mapToDouble(OptionalDouble::getAsDouble)
                                .average()
                ).filter(OptionalDouble::isPresent)
                .mapToDouble(OptionalDouble::getAsDouble)
                .average();

    }


}
